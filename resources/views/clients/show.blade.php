@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Editar Cliente</h1>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-4">
        <form action="{{route('clients.update', $client->id)}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nombre y Apellido:</label>
                <input name="name" type="text" class="form-control @error('name') is-invalid @enderror " value="{{$client->name}}">

                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="phone">Telefono:</label>
                <input name="phone" type="text" class="form-control @error('phone') is-invalid @enderror " value="{{$client->phone}}">

                @error('phone')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input name="email" type="email" class="form-control @error('email') is-invalid @enderror " value="{{$client->email}}">

                @error('email')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="address">Ciudad:</label>
                <input name="address" type="text" class="form-control @error('address') is-invalid @enderror " value="{{$client->address}}">

                @error('address')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div>
                <label for="info">Info Extra:</label>
                <textarea class="form-control mb-3" name="info" cols="8" rows="3">{{$client->info}}</textarea>
            </div>
            {{-- @dd($client->customClients()) --}}
            {{-- @dd($client->customClients()[0]->CustomClient()) --}}
            @if($client->customClients()->count() != 0)
                @foreach ($client->customClients() as $i => $custom_client_pivot)
                        <div class="form-group">
                            <label for="custom_fields[{{$i}}][value]">{{$client->customClients()[$i]->customClient()[0]->value}}</label>
                            <input class="form-control" type="text" name="custom_fields[{{$i}}][value]" value="{{$custom_client_pivot->value}}">
                            <input type="text" hidden name="custom_fields[{{$i}}][client_id]" value="{{$client->id}}">
                            <input type="text" hidden name="custom_fields[{{$i}}][custom_client_id]" value="{{$custom_client_pivot->id}}">
                        </div>
                @endforeach
            @else
                @foreach ($custom_fields as $i => $custom_field)
                    <label for="custom_fields[{{$i}}][value]">{{$custom_field->value}}</label>
                    <input class="form-control mb-3" type="text" name="custom_fields[{{$i}}][value]">
                    <input type="text" hidden name="custom_fields[{{$i}}][client_id]" value="{{$client->id}}">
                    <input type="text" hidden name="custom_fields[{{$i}}][custom_client_id]" value="{{$custom_field->id}}">
                @endforeach
            @endif
            <button class="btn btn-success" type="submit">EDITAR CLIENTE</button>
        </form>
    </div>
</div>
<div class="row">
    <h4>Ventas:</h4>
    {{-- @dd($client->sales) --}}
    @if ($client->sales->count() != 0)
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Ganancia</th>
                    <th scope="col">Info</th>
                    <th scope="col">Fecha de Entrega</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($client->sales as $client_sale)
                    <tr>
                        <th>{{date('d-m-Y', strtotime($client_sale->created_at))}}</th>
                        <td>{{$client_sale->bringStates['name']}}</td>
                        <td>{{$client_sale->cost}}</td>
                        <td>{{$client_sale->revenue}}</td>
                        <td>{{$client_sale->info}}</td>
                        <td>{{$client_sale->date}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
</div>
    <p>Este cliente no tiene ventas.</p>
@endif
@endsection