@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Nuevo Cliente</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Compartir</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Exportar</button>
        </div>
    </div>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-4">
        <form action="{{route('clients.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nombre y Apellido:</label>
                <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name')}}">
                
                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="phone">Telefono:</label>
                <input name="phone" type="text" class="form-control @error('phone') is-invalid @enderror" value=" {{old('phone')}} ">

                @error('phone')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" value=" {{old('email')}} ">

                @error('email')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="address">Ciudad:</label>
                <input name="address" type="text" class="form-control @error('address') is-invalid @enderror" value=" {{old('address')}} ">

                @error('address')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div>
                <label for="info">Info Extra:</label>
                <textarea class="form-control mb-3" name="info" cols="8" rows="3"></textarea>
            </div>
            @foreach ($custom_clients as $i => $custom_client)
                <div class="form-group">
                    <label for="custom_clients[{{$i}}]">{{ucfirst($custom_client->value)}}:</label>
                    <input class="form-control" type="text" name="custom_clients[{{$i}}][value]">
                    <input type="hidden" name="custom_clients[{{$i}}][id]" value="{{$custom_client->id}}">
                </div>
            @endforeach
            <button class="btn btn-success" type="submit">CARGAR CLIENTE</button>
        </form>
    </div>
</div>
@endsection