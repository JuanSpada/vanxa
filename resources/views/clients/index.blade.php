@extends('layouts.app')

@section('content')
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Listado de Clientes</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Ciudad</th>
            <th>Descripción</th>
            @foreach ($custom_fields as $custom_field)
                <th>
                    {{$custom_field->value}}
                </th>   
            @endforeach
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($clients as $client)
            <tr>
                <td>
                    <a href="{{route('clients.show', $client->id)}}">{{$client->id}}</a>
                </td>
                <td>
                    <a href="{{route('clients.show', $client->id)}}">{{$client->name}}</a>
                </td>
                <td>{{$client->phone}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->adress}}</td>
                <td>{{$client->info}}</td>
                @foreach ($client->customClients() as $custom_clients)
                    @if ($client->customClients())
                        <td>
                            {{$custom_clients->value}}
                        </td>   
                    @endif
                @endforeach
                @if($client->customClients()->count() != $custom_fields->count())
                    @foreach ($custom_fields as $nada)
                        <td></td>
                    @endforeach
                @endif
                <td>
                    <div class="row d-flex justify-content-center">
                        
                        <a href="{{route ('clients.show', $client->id)}}"><i class="fas fa-edit"></i></a>
                        <form action="{{route ('clients.destroy')}}" method="GET">
                            <input type="hidden" name="id" value="{{$client->id}}">
                            <button style="border:none; background-color:transparent;" type="submit">
                                <i style="color:#007bff;" class="fas fa-trash"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
@endsection

