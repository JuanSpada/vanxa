@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Editar Proveedor</h1>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-4">
        <form action="{{route('providers.update', $provider->id)}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input name="name" type="text" class="form-control @error('name') is-invalid @enderror " value="{{$provider->name}}">

                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="phone">Telefono:</label>
                <input name="phone" type="text" class="form-control @error('phone') is-invalid @enderror " value="{{$provider->phone}}">

                @error('phone')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="address">Dirección:</label>
                <input name="address" type="text" class="form-control @error('address') is-invalid @enderror " value="{{$provider->address}}">

                @error('address')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="city">Ciudad:</label>
                <input name="city" type="text" class="form-control @error('city') is-invalid @enderror " value="{{$provider->city}}">

                @error('city')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div>
                <label for="info">Detalles:</label>
                <textarea class="form-control mb-3" name="info" cols="8" rows="3">{{$provider->info}}</textarea>
            </div>
            <button class="btn btn-success" type="submit">EDITAR CLIENTE</button>
        </form>
    </div>
</div>
@endsection