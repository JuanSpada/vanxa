@extends('layouts.app')

@section('content')
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          {{-- <h1 class="h3 mb-2 text-gray-800">Tables</h1> --}}
          <button class="btn btn-primary mb-4" data-toggle="modal" data-target="#newProvider">Cargar Proveedor</button>
          {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Listado de Proveedores</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Dirección</th>
            <th>Ciudad</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($providers as $provider)
            <tr>
                <td>
                    <a href="{{route('providers.show', $provider->id)}}">{{$provider->id}}</a>
                </td>
                <td>
                    <a href="{{route('providers.show', $provider->id)}}">{{$provider->name}}</a>
                </td>
                <td>{{$provider->phone}}</td>
                <td>{{$provider->address}}</td>
                <td>{{$provider->city}}</td>
                <td>
                    <div class="row d-flex justify-content-center">
                        <a class="m-2" href="{{route('providers.show', $provider->id)}}">
                            <i class="far fa-eye"></i>
                        </a>
                        <a class="m-2" href="{{route ('providers.edit', $provider->id)}}">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a class="m-2" href="#">
                            <i onclick="destroyProvider({{$provider->id}})" style="color:#007bff;" class="fas fa-trash"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

<!-- Modal -->
<div class="modal fade" id="newProvider" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('providers.store')}}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Cargar Proveedor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" id="" class="form-control">
                    <label for="phone">Teléfono</label>
                    <input type="text" name="phone" id="" class="form-control">
                    <label for="address">Dirección</label>
                    <input type="text" name="address" id="" class="form-control">
                    <label for="city">Ciudad</label>
                    <input type="text" name="city" id="" class="form-control">
                    <label for="info">Detalles:</label>
                    <input type="text" name="info" id="" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
    </div>

    <script>
        function destroyProvider(provider_id)
        {
            fetch('/providers/destroy/' + provider_id, {
			method: 'get',
            }).then(function(response){
                return response.text();
            })
            location.reload();
        }
    </script>

@endsection

