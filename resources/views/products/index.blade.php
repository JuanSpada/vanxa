@extends('layouts.app')

@section('content')
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Listado de Productos</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Costo</th>
                                    <th>Medidas</th>
                                    {{-- <th>Tiempo de Producción</th> --}}
                                    @foreach ($custom_fields as $custom_field)
                                        <th>
                                            {{$custom_field->value}}
                                        </th>   
                                    @endforeach
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                    <tr>
                                        <td>
                                            <a href="{{route('products.show', $product->id)}}">
                                                {{$product->id}}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{route('products.show', $product->id)}}">
                                                {{$product->name}}
                                            </a>
                                        </td>
                                        <td>${{number_format($product->price)}}</td>
                                        <td>${{number_format($product->cost)}}</td>
                                        <td>{{$product->measure}}</td>
                                        {{-- <td>{{$product->time}} Semanas</td> --}}
                                            @foreach ($product->customProducts() as $custom_products)
                                                @if ($product->customProducts())
                                                    <td>
                                                        {{$custom_products->value}}
                                                    </td>   
                                                @endif
                                            @endforeach
                                            @if($product->customProducts()->count() == 0)
                                                @foreach ($custom_fields as $custom_field)
                                                    <td></td>
                                                @endforeach
                                            @endif
                                        <td>
                                            <div class="row d-flex justify-content-center">
                                                <a class="m-2" href="{{route('products.show', $product->id)}}">
                                                    <i class="far fa-eye"></i>
                                                </a>
                                                <a class="m-2" href="{{route ('products.edit', $product->id)}}"><i class="fas fa-edit"></i></a>
                                                <form action="{{route ('products.destroy')}}" method="GET">
                                                    <input type="hidden" name="id" value="{{$product->id}}">
                                                    <button class="m-2" style="border:none; background-color:transparent;" type="submit">
                                                        <i  style="color:#007bff;" class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
@endsection


{{-- @extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Productos</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
        <button type="button" class="btn btn-sm btn-outline-secondary">Compartir</button>
        <button type="button" class="btn btn-sm btn-outline-secondary">Exportar</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
        Esta Semana
        </button>
        <a class="btn btn-sm btn-success ml-2" href="{{route ('products.create')}}">Cargar Producto</a>
    </div>
    </div>
    
    <div class="table-responsive">
    <table id="example" class="display text-center" style="width:100%">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Costo</th>
            <th>Medidas</th>
            <th>Tiempo de Producción</th>
            @foreach ($custom_fields as $custom_field)
                <th>
                    {{$custom_field->value}}
                </th>   
            @endforeach
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{$product->name}}</td>
                <td>${{$product->price}}</td>
                <td>${{$product->cost}}</td>
                <td>{{$product->measure}}</td>
                <td>{{$product->time}} Semanas</td>
                    @foreach ($product->customProducts() as $custom_products)
                        @if ($product->customProducts())
                            <td>
                                {{$custom_products->value}}
                            </td>   
                        @endif
                    @endforeach
                    @if($product->customProducts()->count() == 0)
                        @foreach ($custom_fields as $custom_field)
                            <td></td>
                        @endforeach
                    @endif
                <td>
                    <div class="row d-flex justify-content-center">
                        <a href="{{route ('products.show', $product->id)}}"><i class="fas fa-edit"></i></a>
                        <form action="{{route ('products.destroy')}}" method="GET">
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <button style="border:none; background-color:transparent;" type="submit">
                                <i style="color:#007bff;" class="fas fa-trash"></i>
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable( {
        buttons: [
            'copy', 'excel', 'pdf'
            ]
        } );
        
        table.buttons().container()
            .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
    } );
    </script>
@endsection --}}