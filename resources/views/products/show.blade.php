@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">{{$product->name}}</h1> - <h1>{{$product->measure}}</h1> - <h1>${{$product->price}}</h1>
</div>
<div class="row d-flex justify-content-center">

    <h3>Proveedores:</h3>
</div>
<div class="row">
    @if (count($product->providers()) != 0)
    <hr>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Ciudad</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($product->providers() as $product_provider)
                    {{-- @dd($product_provider->provider()->name) --}}
                    <tr>
                        <th scope="row">{{$product_provider->provider()->id}}</th>
                        <td>${{number_format($product_provider->cost)}}</td>
                        <td>{{$product_provider->provider()->name}}</td>
                        <td>{{$product_provider->provider()->phone}}</td>
                        <td>{{$product_provider->provider()->city}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>Este producto no tiene proveedores</p>
    @endif
</div>
@endsection