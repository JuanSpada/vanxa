@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Nuevo Producto</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Compartir</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Exportar</button>
        </div>
    </div>
</div>
<div class="row d-flex justify-content-center">
    <div class="col-md-10">
        <form action="{{route('products.store')}}" method="POST">
            @csrf
            <div class="col-md-6 m-auto">
                <div class="form-group">
                    <label for="name">Nombre Producto:</label>
                    <input required name="name" type="text" class="form-control @error('name') is-invalid @enderror" value=" {{old('name')}} ">
    
                    @error('name')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="price">Precio:</label>
                    <input required name="price" type="number" class="form-control @error('price') is-invalid @enderror" value=" {{old('price')}} ">
    
                    @error('price')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                {{-- <div class="form-group">
                    <label for="cost">Costo:</label>
                    <input required name="cost" type="text" class="form-control @error('cost') is-invalid @enderror" value=" {{old('price')}} ">
                    
                    @error('cost')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div> --}}
                <div class="form-group">
                    <label for="measure">Medidas:</label>
                    <input required name="measure" type="text" class="form-control">
                </div>
                {{-- <div class="form-group">
                    <label for="time">Tiempo de Producción (en semanas):</label>
                    <input required name="time" type="number" class="form-control">
                </div> --}}
                @foreach ($custom_products as $i => $custom_product)
                    <div class="form-group">
                        <label for="custom_products[{{$i}}]">{{ucfirst($custom_product->value)}}:</label>
                        <input required class="form-control" type="text" name="custom_products[{{$i}}][value]">
                        <input required type="hidden" name="custom_products[{{$i}}][id]" value="{{$custom_product->id}}">
                    </div>
                @endforeach

            </div>

            <div class="row mt-5">
                <div class="col-md-10 m-auto">

                    <div class="row d-flex justify-content-between">
                        <p>Tenes la posibilidad de asignarle diferentes proveedores al producto:</p>

                        {{-- <a class="d-flex align-items-center" href="#" data-toggle="modal" data-target="#addProvider">Agregar Proveedor    <i class="fas fa-plus"></i></a> --}}
                        <a class="d-flex align-items-center" onclick="addProvider()" href="#">Agregar Proveedor    {{-- <i class="fas fa-plus"></i> --}}</a>
                        
                    </div>


                    {{-- <table class="table" id="providerTable">
                        <thead>
                          <tr>
                              <th>Nombre Proveedor</th>
                          </tr>
                        </thead>
                        <tbody id="">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select required class="form-control" name="addProvider[1]">
                                                <option selected disabled value="">Seleccionar</option>
                                                @foreach ($providers as $provider)
                                                    <option value="{{$provider->id}}">{{$provider->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 d-flex flex-row-reverse m-auto">
                                            <a href="#" onclick="removeRow()"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table> --}}
                </div>
            </div>
            <div id="providers-content">
                <div class='row d-flex flex-row'>
                    <label class='m-0 d-flex align-items-center'>Proveedor:</label>
                    <select required class='m-2 form-control form-control-sm col-7' name='providers[${i}][provider_id]'>
                        <option selected disabled>Elegí el Proveedor</option>
                        @foreach ($providers as $provider)
                        <option value='{{$provider->id}}'>{{$provider->name}}</option>
                        @endforeach
                    </select>
                    <label class='m-0 d-flex align-items-center'>Costo:</label>
                    <input required class='m-2 form-control form-control-sm col-1' type='number' name='providers[${i}][cost]' value='' />
                    {{-- <a href="#" class="m-auto" onclick="removeRow(this)"><i class="fas fa-trash-alt"></i></a> --}}
                </div>
            </div>
            <div class="d-flex justify-content-center mt-5">
                <button class="btn btn-success" type="submit">CARGAR PRODUCTO</button>
            </div>

        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="" id="" action="" method="get">
                <div class="modal-header">
                    {{-- <h5 class="modal-title" id="exampleModalLabel">Seleccionar Proveedor</h5> --}}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
            


                    {{-- <input type="text" hidden name="provider_name" id="provider_name" value="{{$provider->name}}">
                    <input type="text" hidden name="provider_phone" id="provider_phone" value="{{$provider->phone}}">
                    <input type="text" hidden name="provider_address" id="provider_address" value="{{$provider->address}}">
                    <input type="text" hidden name="provider_city" id="provider_city" value="{{$provider->city}}"> --}}

                    <select required class="form-control" name="provider_name" id="provider_id">
                        <option value="" selected>Seleccionar Proveedor</option>
                        @foreach ($providers as $provider)
                            <option value="{{$provider->id}}">{{$provider->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    i = 0
    
    function addProvider() {
    ++i
    const div = document.createElement('div');
    div.className = 'row d-flex flex-row';
    div.innerHTML += `
    <label class='m-0 d-flex align-items-center'>Proveedor:</label>
    <select required class='m-2 form-control form-control-sm col-7' name='providers[${i}][provider_id]'>
        <option selected disabled>Elegí el Proveedor</option>
        @foreach ($providers as $provider)
        <option value='{{$provider->id}}'>{{$provider->name}}</option>
        @endforeach
    </select>
    <label class='m-0 d-flex align-items-center'>Costo:</label>
    <input required class='m-2 form-control form-control-sm col-1' type='number' name='providers[${i}][cost]' value='' />
    <a href="#" class="m-auto" onclick="removeRow(this)"><i class="fas fa-trash-alt"></i></a>
    `
    document.getElementById('providers-content').appendChild(div);
    }

    function removeRow(input) {
    var a = document.getElementById('providers-content').removeChild(input.parentNode);
    }
</script>

@endsection