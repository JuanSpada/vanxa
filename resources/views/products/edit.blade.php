@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Editar Producto</h1>
</div>
    <div class="row d-flex justify-content-center">
        <div class="col-md-8">
            <form action="{{route('products.update', $product->id)}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Nombre Producto:</label>
                    <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" required value="{{$product->name}}">

                    @error('name')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="price">Precio:</label>
                    <input name="price" type="number" class="form-control @error('price') is-invalid @enderror" required value="{{$product->price}}">

                    @error('price')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                {{-- <div class="form-group">
                    <label for="cost">Costo:</label>
                    <input name="cost" type="number" class="form-control @error('cost') is-invalid @enderror" value="{{$product->cost}}">

                    @error('cost')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div> --}}
                <div class="form-group">
                    <label for="measure">Medidas:</label>
                    <input name="measure" type="text" class="form-control" value="{{$product->measure}}">
                </div>
                {{-- <div class="form-group">
                    <label for="time">Tiempo de Producción (en semanas):</label>
                    <input name="time" type="number" class="form-control" value="{{$product->time}}">
                </div> --}}
                {{-- @dd($product->customProducts()) --}}
                @if($product->customProducts()->count() != 0)
                    @foreach ($product->customProducts() as $i => $custom_product_pivot)
                            <div class="form-group">
                                <label for="custom_fields[{{$i}}][value]">{{$product->customProducts()[$i]->customProduct()[0]->value}}</label>
                                <input class="form-control" type="text" name="custom_fields[{{$i}}][value]" value="{{$custom_product_pivot->value}}">
                                <input type="text" hidden name="custom_fields[{{$i}}][product_id]" value="{{$product->id}}">
                                <input type="text" hidden name="custom_fields[{{$i}}][custom_product_id]" value="{{$custom_product_pivot->id}}">
                            </div>
                    @endforeach
                @else
                    @foreach ($custom_fields as $i => $custom_field)
                        <label for="custom_fields[{{$i}}][value]">{{$custom_field->value}}</label>
                        <input class="form-control mb-3" type="text" name="custom_fields[{{$i}}][value]">
                        <input type="text" hidden name="custom_fields[{{$i}}][product_id]" value="{{$product->id}}">
                        <input type="text" hidden name="custom_fields[{{$i}}][custom_product_id]" value="{{$custom_field->id}}">
                    @endforeach
                @endif
            </form>
            <a class="btn btn-secondary" href="./">Cancelar</a>
            <button class="btn btn-success" type="submit">Guardar</button>
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-md-12">
            <hr style="background-color:red;">
            <div class="row">
                <h3>Proveedores:</h3>
                <a class="btn btn-primary" style="margin-left:20px;" href="#" data-toggle="modal" data-target="#addProviderModal">
                    Agregar Proveedor
                </a>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($product->providers() as $product_provider)
                    {{-- @dd($product_provider) --}}
                        <tr>
                            <td scope="row">{{$product_provider->id}}</td>
                            <td>${{number_format($product_provider->cost)}}</td>
                            <td>{{$product_provider->name}}</td>
                            <td>{{$product_provider->phone}}</td>
                            <td>{{$product_provider->city}}</td>
                            <td>
                                {{-- @dd($product->hasProviders()) --}}
                                @if(count($product->hasProviders()) > 1)
                                    <form action="{{route('products.provider_destroy', $product_provider->id)}}" method="GET">
                                        <input type="hidden" name="id" value="{{$product_provider->id}}">
                                        <button style="border:none; background-color:transparent;" type="submit">
                                            <i style="color:#007bff;" class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                @else
                                (si o si tiene que tener un proveedor)
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>


    <div class="modal fade" id="addProviderModal" tabindex="-1" role="dialog" aria-labelledby="addProviderModalLabel" aria-hidden="true">
        <form action="" name="addProviderProduct" id="addProviderProduct" method="post">
            @csrf
            <input type="text" hidden value="{{$product->id}}" name="productId" id="productId">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="padding: 15px;">
                    <div class="modal-header">
                    <h5 class="modal-title" id="addProviderModalLabel">Agregar Proveedor a {{$product->name}}:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="row d-flex justify-content-between">
                            <select class="form-control col-md-8" name="provider_id" id="">
                                <option value="null" selected disabled>Proveedor</option>
                                @foreach ($providers as $provider)
                                    <option value="{{$provider->id}}">{{$provider->name}}</option>
                                @endforeach
                            </select>
                            <input type="integer" name="provider_cost" class="form-control col-md-3" placeholder="Costo">
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    <script>
        const addProviderProduct = document.getElementById('addProviderProduct');
        const productId = document.getElementById('productId').value;
        addProviderProduct.addEventListener('submit', function(e) {
            e.preventDefault();
            const formData = new FormData(this);
            fetch('/products/' + productId + '/edit/add-provider', {
                method: 'post',
                body: formData
            }).then(function(response){
                return response.text();
            })
            location.reload();
        })
    </script>
@endsection