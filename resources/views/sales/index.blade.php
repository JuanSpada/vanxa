@extends('layouts.app')

@section('content')
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">VENTAS</h1>
          {{-- <p class="mb-4">Bla bla bla bla.</p> --}}

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Listado de Ventas</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Cliente</th>
                                    <th>Destino</th>
                                    <th>Estado</th>
                                    {{-- <th>Costo</th> --}}
                                    <th>Seña</th>
                                    <th>Ganancia</th>
                                    <th>Source</th>
                                    <th>Fecha de Entrega</th>
                                    @foreach ($custom_fields as $custom_field)
                                        <th>
                                            {{$custom_field->value}}
                                        </th>   
                                    @endforeach
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sales as $sale)
                                    <tr>
                                        {{-- @dd($sale) --}}
                                        <td>{{$sale->id}}</td>
                                        <td>{{$sale->client->name}}</td>
                                        <td>{{$sale->client->adress}}</td>
                                        <td>{{$sale->bringStates['name']}}</td>
                                        <td>${{number_format($sale->downpayment)}}</td>
                                        <td>${{number_format($sale->revenue)}}</td>
                                        <td>{{$sale->source}}</td>
                                        <td>{{$sale->date}}</td>
                                        @foreach ($sale->customSales() as $custom_sale)
                                            @if ($sale->customSales())
                                                <td>
                                                    {{$custom_sale->value}}
                                                </td>   
                                            @endif
                                        @endforeach
                                        @if($sale->customSales()->count() == 0)
                                            @foreach ($custom_fields as $custom_field)
                                                <td></td>
                                            @endforeach
                                        @endif
                                        <td>
                                            <div class="row d-flex justify-content-center">
                                                <a href="{{route ('sales.show', $sale->id)}}"><i class="fas fa-edit"></i></a>
                                                <form action="{{route ('sales.destroy')}}" method="GET">
                                                    <input type="hidden" name="id" value="{{$sale->id}}">
                                                    <button style="border:none; background-color:transparent;" type="submit">
                                                        <i style="color:#007bff;" class="fas fa-trash"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

@endsection