@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Venta de {{$sale->client->name}}</h1>

</div>
<form action="{{route ('sales.update', $sale->id)}}" method="POST"  enctype="multipart/form-data">
    @csrf

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="padding: 15px;">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Producto:</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row d-flex justify-content-between">
                        <select class="form-control col-5" name="add_product" id="">
                            <option value="null" selected disabled>Producto</option>
                            @foreach ($products_all as $product)
                                <option value="{{$product->id}}">{{$product->name}} - {{$product->measure}}</option>
                            @endforeach
                        </select>
                        <input type="integer" name="add_product_ammount" class="form-control col-2" placeholder="Cantidad">
                        <input type="integer" name="add_product_price" class="form-control col-4" placeholder="Precio">
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-striped table-sm text-center">
        <div class="text-center">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Agregar Producto
            </button>
            <br><br>
        </div>
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Proveedor</th>
            <th>Seña Proveedor</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Tiempo</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($products as $i => $product)
            
                <tr>
                    <td>{{$product->name}}</td>
                    {{-- <td>${{$product->cost}}</td> --}}
                    {{-- @dd($product) --}}
                    <td>
                        <select required class="form-control" name="products[{{$i}}][provider_id]" id="products[{{$i}}][provider_id]">
                            <option value="" selected disabled>Seleccionar Proveedor</option>
                            @foreach ($providers as $provider)
                                <option @if($product->provider_id == $provider->id) selected="selected" @endif value="{{$provider->id}}">{{$provider->name}}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        {{-- @dd($product); --}}
                        <input name="products[{{$i}}][downpaymentprovider]" type="number" class="form-control col-md-6 m-auto" value="{{$product->downpaymentprovider}}">
                    </td>
                    <td>${{$product->price}}</td>
                    <input type="text" hidden name="products[{{$i}}][id]" value="{{$product->id}}">
                    <td class="d-flex justify-content-center"><input type="integer" name="products[{{$i}}][ammount]" class="form-control col-2" value="{{$product->ammount}}"></td>
                    <td>{{$product->time}} Semanas</td>
                    <td>
                        <div class="row d-flex justify-content-center">
                            @if($products->count() > 1)
                            <form action="{{route('sales.product_destroy', $product->id)}}" method="GET">
                                @csrf
                                <input type="hidden" name="id" value="{{$product->id}}">
                                <button style="border:none; background-color:transparent;" type="submit">
                                    <i style="color:#007bff;" class="fas fa-trash"></i>
                                </button>
                            </form>
                            @endif
                            <form action=""></form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col form-group">
                    <label for="downpayment">Seña del Cliente</label>
                    <input class="form-control col mb-3" type="integer" name="downpayment" value="{{$sale->downpayment}}">
                    <label class="mb-3" for="satate">Estado de la Venta: {{$sale->bringStates['name']}}</label>
                    <select class="form-control col mb-3" name="state">
                        <option value="" disabled selected>Seleccionar Estado</option>                    
                        @foreach ($states as $state)
                            <option value="{{$state->id}}">{{$state->name}}</option>
                        @endforeach
                    </select>
                    <label for="">Cargar remito</label>
                    <input type="file" class="form-control mb-3" name="img">
                </div>
                <div class="col form-group">
                    @if($sale->customSales()->count() != 0)
                    @foreach ($sale->customSales() as $i => $custom_sale_pivot)
                    <div class="form-group">
                        <label for="custom_fields[{{$i}}][value]">{{$sale->customSales()[$i]->CustomSale()[0]->value}}</label>
                        <input class="form-control col" type="text" name="custom_fields[{{$i}}][value]" value="{{$custom_sale_pivot->value}}">
                        <input type="text" hidden name="custom_fields[{{$i}}][sale_id]" value="{{$sale->id}}">
                        <input type="text" hidden name="custom_fields[{{$i}}][custom_sale_id]" value="{{$custom_sale_pivot->id}}">
                    </div>
                    @endforeach
                    @else
                    @foreach ($custom_fields as $i => $custom_field)
                    <label for="custom_fields[{{$i}}][value]">{{$custom_field->value}}</label>
                    <input class="form-control mb-3 col" type="text" name="custom_fields[{{$i}}][value]">
                    <input type="text" hidden name="custom_fields[{{$i}}][sale_id]" value="{{$sale->id}}">
                    <input type="text" hidden name="custom_fields[{{$i}}][custom_sale_id]" value="{{$custom_field->id}}">
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="col-6 form-group">
            <div class="row">
                @if($sale->img != null)
                    <img src="{{url('storage/' . $sale->img)}}" width="250px" style="margin-left: 35px;">
                @else
                    <img src="https://www.union.edu/sites/default/files/union-marketing-layer/201803/picture.jpg" alt="" style="margin-left: 35px;" width="250px">
                @endif
            </div>
        </div>
    </div>
</form>
<button type="submit" class="btn-primary btn btn-sm">GUARDAR</button>
<a href="./" class="btn btn-secondary btn-sm">CANCELAR</a>
@endsection
