@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Nueva Venta</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Compartir</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Exportar</button>
        </div>
    </div>
</div>
<div class="row d-flex justify-content-center">
    <div class="container">
        <div class="col">

            <form action="{{ route('sales.create') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{Auth::user()->company_id}}" name="company_id">
                <div class="row mt-3">    
                    <div class="col-3" style="padding-left: 0;">
                        <label for="client">Cliente: </label>
                        <select id="" class="form-control @error('client_id') is-invalid @enderror " name="client_id">
                            <option value="" selected disabled>Elegir cliente</option>
                            @foreach ($clients as $client)
                                <option value="{{$client->id}}">{{$client->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-2">
                        <label for="downpayment">Seña: </label>
                        <input type="text" name="downpayment" class="form-control @error('downpayment') is-invalid @enderror">
                    </div>

                    <div class="col-2">
                        <label for="source">Fuente: </label>
                        <select name="source" id="" class="form-control">
                            <option value="referido" selected>Orgánico</option>
                            <option value="facebook">Facebook</option>
                            <option value="instagram">Instagram</option>
                            <option value="referido">Referido</option>
                        </select>
                    </div>
                    
                    {{-- <div class="col-2">
                        <label class="" for="state">Estado:</label>
                        <select name="state" class="form-control">
                            <option value="" selected disabled>Elegir Estado</option>
                            @foreach ($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                    </div> --}}

                    <div class="col-3" style="padding-right: 0;">
                        <label for="remito">Remito: </label>
                        <input type="file" class="form-control" name="img">
                    </div>
                </div>
                <div class="row mt-3">
                    <p>Productos <a onclick="addProduct()" href="#"><i class="fas fa-plus fa-lg"></i></a></p>
                </div>
                
                <div id="product-content">
                    <div class='row d-flex flex-row'>
                        <label class='m-0 d-flex align-items-center'>Nombre:</label>
                        <select class='m-2 form-control form-control-sm col-7' name='products[1][product_id]'>
                            <option selected disabled>Elegí el Producto</option>
                            @foreach ($products as $product)
                            <option value='{{$product->id}}'>{{$product->name}} - {{$product->measure}}</option>
                            @endforeach
                        </select>
                        <label class='m-0 d-flex align-items-center'>Cantidad:</label>
                        <input class='m-2 form-control form-control-sm col-1' type='number' name='products[1][ammount]' value='' />
                        <a href="#" class="m-auto" onclick="removeRow(this)"><i class="fas fa-trash-alt"></i></a>
                    </div>
                </div>


                <hr>
                <div class="row mt-3" style="width: 40%">
                    <label for="info">Info Extra:</label>
                    <textarea required class="form-control" name="info" rows="3"></textarea>
                </div>
                @foreach ($custom_sales as $i => $custom_sale)
                    <div class="form-group">
                        <label for="custom_sales[{{$i}}]">{{ucfirst($custom_sale->value)}}:</label>
                        <input class="form-control" type="text" name="custom_sales[{{$i}}][value]">
                        <input type="hidden" name="custom_sales[{{$i}}][id]" value="{{$custom_sale->id}}">
                    </div>
                @endforeach
                <div class="row">
                    <button type="submit" class="btn btn-success mt-3">CARGAR VENTA</button>
                </div> 
            </form>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    var i = 1;
function addProduct() {
    ++i
    const div = document.createElement('div');
    div.className = 'row d-flex flex-row';
    div.innerHTML += `
    <label class='m-0 d-flex align-items-center'>Nombre:</label>
    <select class='m-2 form-control form-control-sm col-7' name='products[${i}][product_id]'>
        <option selected disabled>Elegí el Producto</option>
        @foreach ($products as $product)
        <option value='{{$product->id}}'>{{$product->name}} - {{$product->measure}}</option>
        @endforeach
    </select>
    <label class='m-0 d-flex align-items-center'>Cantidad:</label>
    <input class='m-2 form-control form-control-sm col-1' type='number' name='products[${i}][ammount]' value='' />
    <a href="#" class="m-auto" onclick="removeRow(this)"><i class="fas fa-trash-alt"></i></a>
    `
    document.getElementById('product-content').appendChild(div);
}

function removeRow(input) {
  var a = document.getElementById('product-content').removeChild(input.parentNode);
}
</script>