@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Venta de {{$sale->client->name}}</h1>

</div>
<table class="table table-striped table-sm text-center">
    <div class="d-flex justify-content-between">
        <div class="">
            <p>Productos</p>
        </div>
        <div class="">
            <a class="btn btn-primary btn-sm" href="{{route ('sales.edit', $sale->id)}}">EDITAR VENTA</a>
        </div>
    </div>
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Costo</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Ganancia</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
        <tr>
            <td>{{$product->name}}</td>
            {{-- <td>${{number_format($product->cost * $product->ammount)}}</td> --}}
            <td>1</td>
            <td>${{number_format ($product->price * $product->ammount)}}</td>
            <td>{{$product->ammount}} (unidades)</td>
            {{-- <td>{{number_format(($product->price*$product->ammount)-($product->cost*$product->ammount))}}</td> --}}
            <td>2</td>
        </tr>
        @endforeach
        <tr>
            <th>TOTAL</th>
            <th>${{number_format($total_cost)}}</th>
            <th>${{number_format($total_price)}}</th>
            <th></th>
            <th>${{number_format($sale->revenue)}}</th>
        </tr>
    </tbody>
</table>

<p>SEÑA: ${{number_format($sale->downpayment)}}</p>
<p>TOTAL VENTA: ${{number_format($total_price)}}</p>
<p>COSTO TOTAL: ${{number_format($total_cost)}}</p>
<p>GANANCIA: ${{number_format($sale->revenue)}}</p>
<p>ESTADO: {{$sale->bringStates['name']}}</p>

@if($sale->img)
    <img src="{{url('storage/' . $sale->img)}}" alt="">
@else
    <img src="https://www.union.edu/sites/default/files/union-marketing-layer/201803/picture.jpg" alt="">
@endif
@if($sale->customSales()->count() != 0)
@foreach ($sale->customSales() as $i => $custom_sale_pivot)
    <p>{{$sale->customSales()[$i]->CustomSale()[0]->value}}: {{$custom_sale_pivot->value}}</p>
    @endforeach
    @else
    @foreach ($custom_fields as $custom_field)
    <p>{{$custom_field->value}}: </p>
    @endforeach
@endif

@endsection