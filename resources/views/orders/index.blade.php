    @extends('layouts.app')

    @section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">ENTREGAS</h1>
        {{-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> --}}

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="get" name="stateEdit" id="stateEdit">
                            <div class="row">
                                <div class="col-3">
                                    <select class="form-control" name="custom_state" id="custom_state">
                                        <option @if(app('request')->input('custom_state') == null) selected="selected" @endif disabled value="">Seleccionar</option>
                                        @foreach ($states as $state)
                                            <option @if($state->id == app('request')->input('custom_state')) selected="selected" @endif value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <button class="btn btn-secondary" type="submit">Filtrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#filterModal">FILTRAR PEDIDOS</button> --}}
            </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Estado</th>
                            <th>Emisión</th>
                            <th>Cliente</th>
                            <th>Destino</th>
                            <th>Fecha de Entrega</th>
                            @foreach ($custom_fields as $custom_field)
                                <th>
                                    {{$custom_field->value}}
                                </th>   
                            @endforeach
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($sales as $sale)
                        <tr>
                            <td>{{$sale->id}}</td>
                            <td>
                                <form action="" method="post" name="stateEdit" id="{{$sale->id}}stateEdit">
                                    @csrf
                                    <select onchange="stateedit({{$sale->id}})" class="form-control" name="state">
                                        @foreach ($states as $state)
                                        {{-- tengo q mandar al js el value del saate pero nose como --}}
                                            <option @if($sale->state == $state->id) selected="selected" @endif value="{{$state->id}}">{{$state->name}}</option>
                                        @endforeach
                                    </select>   

                                </form>
                            </td>
                            <td>{{$sale->created_at->format('d M Y')}}</td>
                            <td>{{$sale->client->name}}</td>
                            <td>{{$sale->client->adress}}</td>
                            <td>{{$sale->date}}</td>
                            @foreach ($sale->customSales() as $custom_sale)
                                @if ($sale->customSales())
                                    <td>
                                        {{$custom_sale->value}}
                                    </td>   
                                @endif
                            @endforeach
                            @if($sale->customSales()->count() == 0)
                                @foreach ($custom_fields as $custom_field)
                                    <td></td>
                                @endforeach
                            @endif
                            <td>
                                <div class="row d-flex justify-content-center">
                                    <a href="{{route ('sales.show', $sale->id)}}"><i class="fas fa-edit"></i></a>
                                    <form action="{{route ('sales.destroy')}}" method="GET">
                                        <input type="hidden" name="id" value="{{$sale->id}}">
                                        <button style="border:none; background-color:transparent;" type="submit">
                                            <i style="color:#007bff;" class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>

    <script>
        function stateedit(sale_id){
            console.log(sale_id)
            var form = document.getElementById(sale_id + 'stateEdit')
            var formData = new FormData(form);
            formData.append('sale_id', sale_id);
            // formData.append('sale_id', sale);
            var csrfToken = window.CSRF_TOKEN = '{{ csrf_token() }}';
            var headers = new Headers({
                'X-CSRF-TOKEN': csrfToken,
                });
            fetch('/orders', {
                method: 'post',
                headers,
                body: formData
            }).then(function(response){
                return response.text();
            })
            location.reload();
        }
    </script>
@endsection