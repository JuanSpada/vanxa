@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Personalizar</h1>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.customize.clients.')}}">Clientes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.customize.products.')}}">Productos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.customize.sales.')}}">Ventas</a>
    </li>
</ul>
<div class="d-flex justify-content-center align-content-center align-items-center" style="height:50vh;">
    <p>Personaliza los Campos del sistema a tu medida.</p>
</div>

@endsection
