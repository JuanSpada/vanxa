@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Personalizar Campos Ventas</h1>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.customize.clients.')}}">Clientes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('settings.customize.products.')}}">Productos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="{{route('settings.customize.sales.')}}">Ventas</a>
    </li>
</ul>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-sm mt-3" data-toggle="modal" data-target="#exampleModal">
  Cargar Campo Nuevo
</button>
<p>Campos de la Venta:</p>
<ul>
    <li>Costo (no editable)</li>
    <li>Info (no editable)</li>
    <li>Seña (no editable)</li>
    @foreach ($custom_sales as $custom_sale)
        <li>{{$custom_sale->value}} | <a href="#" onclick="customValidate({{$custom_sale->id}})">Eliminar</a></li>
    @endforeach
</ul>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nuevo Campo Venta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('settings.customize.sales.store')}}" method="POST">
        @csrf
          <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <label for="value">Nombre</label>
                        <input type="text" class="form-control" name="value">
                    </div>
                    <div class="col-6">
                        <label for="value">Tipo de Dato</label>
                        <select required class="form-control" name="data_type">
                            <option value="" disabled selected>Seleccione el Tipo de Dato</option>
                            <option value="1">Número Entero</option>
                            <option value="2">Texto</option>
                            <option value="3">Número Decimal</option>
                            <option value="4">Verdadero o Falso</option>
                        </select>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>
@endsection
<script>
function customValidate(custom_sale) {
    swal({
        title: "Estás Seguro?",
        text: "Se borraran todos los datos que esten asignados a esta propiedad!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            swal("Listo", {
            icon: "success",
            })
            window.location = `/settings/customize/sales/${custom_sale}/destroy`
        } else {
            swal("El campo NO fue eliminado!");
        }
    });
}
</script>