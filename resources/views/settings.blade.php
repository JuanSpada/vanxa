@extends('layouts.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Ajustes</h1>
</div>
    
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="state-tab" data-toggle="tab" href="#state" role="tab" aria-controls="state" aria-selected="true">Estados</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="adduser-tab" data-toggle="tab" href="#adduser" role="tab" aria-controls="adduser" aria-selected="false">Agregar Usuario</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="perfil" aria-selected="false">Perfil</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="company-tab" data-toggle="tab" href="#company" role="tab" aria-controls="company" aria-selected="false">Empresa</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="state" role="tabpanel" aria-labelledby="state-tab">
        <p class="mt-3">Editar los estados de la venta:</p>
        <form action="{{route('settings.settings')}}" method="POST">
            @csrf
            <div class="col-3 form-group">
                <label for="name">Nombre:</label>
                <input type="text" class="form-control" name="state-name">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Guardar</button>
        </form>
        @if($states->count() > 0)
            <p class="mt-3">Estados: </p>
            <ul>
                @foreach ($states as $state)
                    @if ($state->name == "Nuevo" | $state->name == "En Progreso" | $state->name == "Entregado" )
                        <li>{{$state->name}} (no editable)</li>
                    @else
                        <li>{{$state->name}}  |  <a href="{{route('settings.states.destroy', $state->id)}}">Eliminar</a></li>
                    @endif
                @endforeach
            </ul>
        @else
            <div class="alert alert-danger col-3 mt-3" role="alert">
                Todavía no tenes estados.
            </div>
        @endif
    </div>
    <div class="tab-pane fade" id="adduser" role="tabpanel" aria-labelledby="adduser-tab">
        <p class="mt-3">Invitar Usuario a Empresa:</p>
        <form action="{{route('settings.settings')}}" method="POST">
            @csrf
            <label for="invite-email">Email</label>
            <input class="form-control col-4 mb-3" type="email" name="invite-email">
            <button class="btn btn-sm btn-primary" type="submit">Invitar</button>
        </form>
    </div>
    <div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">
        <p class="mt-3">Editar Perfil:</p>
        <form class="col-4" action="{{route('settings.settings')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="row rounded">
                    <img style="border-radius: 50%; border: 1px solid lightgrey;" width= "200px" height="200px" src="{{url('storage/' . Auth::user()->img)}}" alt=""> 
                </div>
                <label for="user-image"></label>
                <input name="user-image" class="form-control" type="file" style="width: 46%;">
            </div>
            <div class="form-group">
                <label for="user-name">Nombre</label>
                <input type="text" class="form-control" name="user-name" value="{{Auth::user()->name}}">
            </div>
            <div class="form-group">
                <label for="user-email">Email</label>
                <input type="text" class="form-control" name="user-email" value="{{Auth::user()->email}}">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Editar</button>
        </form>
    </div>
    <div class="tab-pane fade" id="company" role="tabpanel" aria-labelledby="company-tab">
        <p class="mt-3">Editar Empresa:</p>
        <form class="col-4" action="{{route('settings.settings')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="row rounded">
                    <img style="border-radius: 50%; border: 1px solid lightgrey;" width= "200px" height="200px" src="{{url('storage/' . Auth::user()->company->img)}}" alt=""> 
                </div>
                <label for="company-image"></label>
                <input name="company-image" class="form-control" type="file" style="width: 46%;">
            </div>
            <div class="form-group">
                <label for="company-name">Nombre Empresa</label>
                <input type="text" class="form-control" name="company-name" value="{{Auth::user()->company->name}}">
            </div>
            <button type="submit" class="btn btn-sm btn-primary">Editar</button>
        </form>
    </div>
</div>
@endsection
