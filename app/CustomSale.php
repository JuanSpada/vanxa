<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomSale extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'value', 'data_type',
    ];

    public function sale(){
        return $this->belongsTo('App\Sale');
    }
}
