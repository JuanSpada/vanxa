<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sale;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'company_id',
    ];

    public function sales(){
        return $this
        ->hasMany(Sale::class);
    }
}
