<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\State;
use App\CustomSale_pivot;
use Auth;

class Sale extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'client_id', 'downpayment', 'cost', 'date', 'revenue', 'source', 'state', 'info', 'downpayment_provider', 'price', 'img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function client(){
        return $this->belongsTo('App\Client');
    }

    public function products(){
        return $this
        ->belongsToMany(Product::class, 'product_sales');
        // ->belongsToMany(Product::class);
    }

    public function bringStates(){
        return $this->belongsTo(State::class, 'state');
    }

    // public function customSale(){
    //     return $this->belongsToMany('App\CustomSale');
    // }

    public function customSales(){
        $custom_sales = CustomSale_pivot::where('sale_id', $this->id)->get();
        return $custom_sales;
    }

}