<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'img',
    ];

    // public function user(){
    //     return $this->hasMany('App\User');
    // }
    public function user(){
        return $this->hasMany(User::class, 'company_id');
    }
}
