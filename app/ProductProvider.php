<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProvider extends Model
{
    public function provider(){
        $provider = Provider::find($this->id);
        return $provider;
    }
}
