<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'phone', 'email', 'adress', 'info'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function sales(){
        return $this->hasMany('App\Sale');
    }

    public function customClient(){
        return $this->belongsToMany('App\CustomClient', 'company_id');
    }
    public function customClients(){
        $custom_clients = CustomClient_pivot::where('client_id', $this->id)->get();
        return $custom_clients;
    }
}