<?php

namespace App\Http\Controllers;

use App\Sale;
use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Client;
use App\ProductSale;
use App\State;
use DB;
use App\CustomSale;
use App\CustomSale_pivot;
use Carbon\Carbon;
use App\Provider;
use App\ProductSaleProvider_pivot;
use App\ProductProviders;

class SaleController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sale::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->get();
        $custom_fields = CustomSale::where('company_id', Auth::user()->company_id)->get();
        return view('sales.index')
        ->with('sales', $sales)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::where('company_id', Auth::user()->company_id)->get();
        $clients = Client::where('company_id', Auth::user()->company_id)->get();
        $states = State::where('company_id', Auth::user()->company_id)->get();
        $custom_sales = CustomSale::where('company_id', Auth::user()->company_id)->get();
        return view('sales.create')
        ->with('products', $products)
        ->with('clients', $clients)
        ->with('states', $states)
        ->with('custom_sales', $custom_sales);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if($request->img){
            $route = $request->img->store('public');
            $fileName = basename($route);            
        } else{ $fileName = "";}

        $messages = [
            'client_id.required' => 'Este campo es obligatorio',
            'downpayment.required' => 'Este campo es obligatorio',
            'downpayment.numeric' => 'Este campo solo puede contener numero',
        ];

        $validation = $request->validate([
            'client_id' => 'required',
            'downpayment' => 'required|numeric',
            'source' => 'nullable',
            'products' => 'required',
            'img' => 'nullable'
        ], $messages);

        $sale_state = 1;
        if ($request->sate = null) {
            $sale_state = $request->state;
        }

        /* Primero creamos la venta */
        $sale = new Sale;
        $sale->company_id = Auth::user()->company_id;
        $sale->client_id = $request->client_id;
        $sale->downpayment = $request->downpayment;
        $sale->source = $request->source;
        $sale->info = $request->info;
        $sale->state = $sale_state;
        $sale->img = $fileName;
        $sale->save();
        /* Ahora creamos la tabla PIVOT ProductSales */

        
        foreach ($request->products as $product) {
            $product_id = (int)$product['product_id'];

            // Buscamos el producto para sacar el precio
            $product_price = Product::find($product_id);

            $product_sale = new ProductSale;
            $product_sale->company_id = Auth::user()->company_id;
            $product_sale->sale_id = (int)$sale->id;
            $product_sale->product_id = (int)$product['product_id'];
            $product_sale->ammount = (int)$product['ammount'];
            $product_sale->price = (int)$product_price->price;
            $product_sale->save();
            
        }
        foreach ($sale->products as $product) {
            $products = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.product_id')
            ->where('sale_id', $sale->id)
            ->select('products.name', 'products.price', 'products.measure', 'products.name', 'products.time', 'product_sales.ammount', 'product_sales.id')->get();
        }
        //guardamos la ganancia
        // $total_cost = 0;
        // $total_price = 0;
        // foreach ($products as $product) {
        //     $total_cost += $product->cost*$product->ammount;
        //     $total_price += $product->price*$product->ammount;
        // }
        // $revenue = $total_price - $total_cost;
        // $sale->revenue = $revenue;
        // $sale->save();
        
        /* Calculamos la fecha de entrega */
        // foreach ($sale->products as $i => $sale_product) {
        //     $time[$i]['time'] = $sale_product->time;
        //     $time[$i]['product_id'] = $sale_product->id;
        // }
        // $max_time = max($time);
        // $max_time = $max_time['time']*7;
        // $date = Carbon::now();
        // $date->add($max_time, 'days');
        // $sale->date = $date;
        // $sale->save();
        /*  */

        foreach ($request->products as $i => $product) {
            /* Ahora agregamos el costo  al sale*/
            $costo = 0;
            foreach ($sale->products as $sale_product) {
                $costo += $sale_product->cost*$product['ammount'];
                $sale->cost = $costo;
                $sale->save();
            }

            /* Ahora agregamos la ganancia al sale*/
            // $precio = 0;
            // foreach ($sale->products as $sale_product) {
            //     $precio += $sale_product->price * $product['ammount'];
            // }
            // // dd($product['ammount']);
            // $ganancia = $precio - $costo;
            // $sale->revenue = $ganancia;
            // $sale->save();
        }

        if($request->custom_sales){
            /* CREAMOS LOS PRODUCTS VALUES EN LA TABLA PIVOT */
            foreach ($request->custom_sales as $i => $custom_sale) {
            $custom_sale_pivot = new CustomSale_pivot;
            $custom_sale_pivot->custom_sale_id = $custom_sale['id'];
            $custom_sale_pivot->company_id = Auth::user()->company_id;
            $custom_sale_pivot->value = $custom_sale['value'];
            $custom_sale_pivot->sale_id = $sale->id;
            $custom_sale_pivot->save();
            }
        }

        $custom_sales = CustomSale::where('company_id', Auth::user()->company_id)->get();

        return redirect('sales')
        ->with('custom_sales', $custom_sales);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $sale = Sale::find($id);
        foreach ($sale->products as $product) {
            $products = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.product_id')
            ->where('sale_id', $id)
            ->select('products.name', 'product_sales.price', 'products.measure', 'products.name', 'products.time', 'product_sales.ammount', 'products.id')->get();
        }
        $products_all = Product::where('company_id', Auth::user()->company_id)->get();
        $total_cost = 0;
        $total_price = 0;
        foreach ($products as $product) {
            // $total_cost += $product->cost*$product->ammount;
            $total_price += $product->price*$product->ammount;
        }
        $custom_fields = CustomSale::where('company_id', Auth::user()->company_id)->get();
        $product_sales = ProductSale::where('sale_id', $sale->id)->get();

        if (Auth::user()->company_id == $sale->company_id){
            return view('sales.show')
            ->with('products', $products)
            ->with('sale', $sale)
            ->with('products_all', $products_all)
            ->with('total_cost', $total_cost)
            ->with('total_price', $total_price)
            ->with('product_sales', $product_sales)
            ->with('custom_fields', $custom_fields);
        }else{
            abort(403);
        }

        // return view('sales.show')
        // ->with('products', $products)
        // ->with('sale', $sale)
        // ->with('products_all', $products_all)
        // ->with('total_cost', $total_cost)
        // ->with('total_price', $total_price)
        // ->with('product_sales', $product_sales)
        // ->with('custom_fields', $custom_fields);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::find($id);
        foreach ($sale->products as $product) {
            $products = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.product_id')
            ->where('sale_id', $id)
            ->select('products.name', 'product_sales.provider_id', 'product_sales.downpaymentprovider' , 'product_sales.price', 'products.measure', 'products.name', 'products.time', 'product_sales.ammount', 'product_sales.id')->get();
        }
        $custom_fields = CustomSale::where('company_id', Auth::user()->company_id)->get();
        $products_all = Product::where('company_id', Auth::user()->company_id)->get();
        $states = State::where('company_id', Auth::user()->company_id)->get();
        $providers = Provider::where('company_id', Auth::user()->company_id)->get();
        
        
        // return view('sales.edit')
        // ->with('products', $products)
        // ->with('sale', $sale)
        // ->with('products_all', $products_all)
        // ->with('states', $states)
        // ->with('custom_fields', $custom_fields);

        if (Auth::user()->company_id == $sale->company_id){
            return view('sales.edit')
            ->with('products', $products)
            ->with('sale', $sale)
            ->with('products_all', $products_all)
            ->with('states', $states)
            ->with('providers', $providers)
            ->with('custom_fields', $custom_fields);
        }else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        if($request->img){
            $route = $request->img->store('public');
            $fileName = basename($route);            
        } else{ $fileName = "";}


        /* Si agrego un producto a la venta */
        if($request->add_product){
            /* Buscamos la venta */
            $sale = Sale::find($id);
            /* Creamos un ProductSale (la tabla pivot) */
            $product_sale = new ProductSale;
            $product_sale->company_id = Auth::user()->company_id;
            $product_sale->sale_id = $sale->id;
            $product_sale->product_id = $request->add_product;
            $product_sale->ammount = $request->add_product_ammount;
            $product_sale->price = $request->add_product_price;
            $product_sale->save();
            return back();
        }
        
        /*  */
        $sale = Sale::find($id);
        foreach ($request->products as $product) {
            $product_sale = ProductSale::find($product['id']);
            $product_sale->ammount = (int)$product['ammount'];
            $product_sale->provider_id = (int)$product['provider_id'];
            $product_sale->downpaymentprovider = $product['downpaymentprovider'];
            $product_sale->provider_id = $product['provider_id'];
            dd($product_sale->productProviders());
            $product_sale->save();


            //GUARDAMOS EL PRODUCT_SALE_PROVIDER para guardar el proveedor de cada productsale y poder calcular el costo y la ganancia
    
            // $product_sale_provider = new ProductSaleProvider_pivot;
            // $product_sale_provider->company_id = Auth::user()->company_id;
            // $product_sale_provider->product_sales_id = $product_sale->id;
            // $product_sale_provider->provider_id = $product['provider_id'];
            // $product_sale_provider->product_sales_id = $product_sale->id;
            // $product_sale_provider->save();
        }

    




        foreach ($sale->products as $product) {
            $products = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.product_id')
            ->where('sale_id', $id)
            ->select('products.name', 'product_sales.provider_id', 'product_sales.downpaymentprovider' , 'product_sales.price', 'products.measure', 'products.name', 'products.time', 'product_sales.ammount', 'product_sales.id')->get();
        }

        // dd($product_sale_provider);


        $sale->downpayment = (int)$request->downpayment;
        $sale->state = $request->state;
        $sale->img = $fileName;
        
        $sale->save();
        foreach ($sale->products as $product) {
            $products = DB::table('product_sales')
            ->join('products', 'products.id', '=', 'product_sales.product_id')
            ->where('sale_id', $id)
            ->select('products.name', 'products.price', 'products.measure', 'products.name', 'products.time', 'product_sales.ammount', 'product_sales.id')->get();
        }



        // dd($request->products);
        // CALCULAMOS LA GANANCIA
        $total_cost = 0;
        $total_price = 0;
        foreach ($products as $product) {
            $total_cost += $product->cost*$product->ammount;
            $total_price += $product->price*$product->ammount;
        }
        $revenue = $total_price - $total_cost;

        /* Guardando la ganancia */
        $sale = Sale::find($id);
        $sale->revenue = $revenue;
        $sale->save();
 
        /* EDITAMOS EL PIVOT TABLE */
        if($request->custom_fields){
            foreach ($request->custom_fields as $custom_field) {
                $custom_sale_pivot = CustomSale_pivot::where([
                    ['sale_id', $custom_field['sale_id']],
                    ['id', $custom_field['custom_sale_id']],
                ])->get();
                $custom_sale_pivot[0]->value = $custom_field['value'];
                $custom_sale_pivot[0]->save();
            }
        }

        $custom_fields = CustomSale::where('company_id', Auth::user()->company_id);

        return view('sales.show')
        ->with('sale', $sale)
        ->with('products', $products)
        ->with('total_cost', $total_cost)
        ->with('total_price', $total_price)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $sale = Sale::find($request->id);
        $sale->delete();

        $product_sales = ProductSale::where('sale_id', $sale->id)->get();

        foreach ($product_sales as $product_sale) {
            $product_sale->delete();
        }
        // dd($product_sales);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
     public function orders(Request $request)
    {
        $custom_fields = CustomSale::where('company_id', Auth::user()->company_id)->get();
        $custom_state = $request->get('custom_state');
        $states =  State::where('company_id', Auth::user()->company_id)
                        ->get();

        if ($custom_state) {
            $sales = Sale::where('company_id', Auth::user()->company_id)
            ->where('state', $custom_state)
            ->get();
        }else {
            $sales = Sale::where('company_id', Auth::user()->company_id)
                            ->get();
        }

        return view('orders.index')
        ->with('sales', $sales)
        ->with('custom_fields', $custom_fields)
        ->with('states', $states);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
     public function orderUpdate(Request $request)
    {
        $sale_id = $request->sale_id;
        $state = $request->state;
        // dd($state);
        $sale = Sale::find($sale_id);
        // dd($sale->client->name);
        $sale->state = $state;
        $sale->save();
    }
    


}
