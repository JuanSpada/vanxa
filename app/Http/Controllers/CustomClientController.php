<?php

namespace App\Http\Controllers;

use App\CustomClient;
use Illuminate\Http\Request;
use Auth;
use App\Client;
use App\CustomClient_pivot;

class CustomClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custom_clients = CustomClient::where('company_id', Auth::user()->id)->get();
        return view('customize.clients.index')
                ->with('custom_clients', $custom_clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* Creamos la tabla del customfield */
        $custom_client = new CustomClient;
        $custom_client->company_id = Auth::user()->company_id;
        $custom_client->value = $request->value;
        $custom_client->data_type = $request->data_type;
        $custom_client->save();

        /* Creamos los pivots para los clientes ya existentes */
        $clients = Client::where('company_id', Auth::user()->company_id)->get();
        foreach ($clients as $client) {
            $custom_client_pivot = new CustomClient_pivot;
            $custom_client_pivot->custom_client_id = $custom_client->id;
            $custom_client_pivot->company_id = Auth::user()->company_id;
            $custom_client_pivot->value = null;
            $custom_client_pivot->client_id = $client->id;
            $custom_client_pivot->save();
        }
        return back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomClient  $customClient
     * @return \Illuminate\Http\Response
     */
    public function show(CustomClient $customClient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomClient  $customClient
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomClient $customClient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomClient  $customClient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomClient $customClient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomClient  $customClient
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomClient $customClient, $id)
    {
        $custom_client = CustomClient::find($id);
        $custom_client_pivots = CustomClient_pivot::where([
            ['company_id', Auth::user()->company_id],
            ['custom_client_id', $id],
        ])->get();
        foreach ($custom_client_pivots as $custom_client_pivot) {
            $custom_client_pivot->delete();
        }
        $custom_client->delete();
        return back();
    }
}
