<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\State;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'company' => ['required', 'string', 'min:2']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $company = new Company;
        $company->name = $data['company'];
        $company->save();

        /* Creamos estados default */
        $state1 = new State;
        $state1->name = 'En Progreso';
        $state1->company_id = $company->id;
        $state1->save();

        $state2 = new State;
        $state2->name = 'Nuevo';
        $state2->company_id = $company->id;
        $state2->save();

        $state1 = new State;
        $state1->name = 'Entregado';
        $state1->company_id = $company->id;
        $state1->save();

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'company_id' => $company->id,
            'img' => 'provo.jpg',
            'password' => Hash::make($data['password']),
        ]);

    }
}
