<?php

namespace App\Http\Controllers;

use App\ProductSaleProvider_pivot;
use Illuminate\Http\Request;

class ProductSaleProviderPivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductSaleProvider_pivot  $productSaleProvider_pivot
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSaleProvider_pivot $productSaleProvider_pivot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductSaleProvider_pivot  $productSaleProvider_pivot
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductSaleProvider_pivot $productSaleProvider_pivot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductSaleProvider_pivot  $productSaleProvider_pivot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSaleProvider_pivot $productSaleProvider_pivot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductSaleProvider_pivot  $productSaleProvider_pivot
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductSaleProvider_pivot $productSaleProvider_pivot)
    {
        //
    }
}
