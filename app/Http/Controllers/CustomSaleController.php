<?php

namespace App\Http\Controllers;

use App\CustomSale;
use Illuminate\Http\Request;
use Auth;
use App\Sale;
use App\CustomSale_pivot;

class CustomSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custom_sales = CustomSale::where('company_id', Auth::user()->id)->get();
        return view('customize.sales.index')
                ->with('custom_sales', $custom_sales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_sale = new CustomSale;
        $custom_sale->company_id = Auth::user()->company_id;
        $custom_sale->value = $request->value;
        $custom_sale->data_type = $request->data_type;
        $custom_sale->save();

        /* Creamos los pivots para los clientes ya existentes */
        $sales = Sale::where('company_id', Auth::user()->company_id)->get();
        foreach ($sales as $sale) {
            $custom_sale_pivot = new CustomSale_pivot;
            $custom_sale_pivot->custom_sale_id = $custom_sale->id;
            $custom_sale_pivot->company_id = Auth::user()->company_id;
            $custom_sale_pivot->value = null;
            $custom_sale_pivot->sale_id = $sale->id;
            $custom_sale_pivot->save();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomSale  $customSale
     * @return \Illuminate\Http\Response
     */
    public function show(CustomSale $customSale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomSale  $customSale
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomSale $customSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomSale  $customSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomSale $customSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomSale  $customSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomSale $customSale, $id)
    {
        $custom_sale = CustomSale::find($id);
        $custom_sale_pivots = CustomSale_pivot::where([
            ['company_id', Auth::user()->company_id],
            ['custom_sale_id', $id],
        ])->get();
        foreach ($custom_sale_pivots as $custom_sale_pivot) {
            $custom_sale_pivot->delete();
        }
        $custom_sale->delete();
        return back();
    }
}
