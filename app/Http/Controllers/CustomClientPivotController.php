<?php

namespace App\Http\Controllers;

use App\CustomClient_pivot;
use Illuminate\Http\Request;

class CustomClientPivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomClient_pivot  $customClient_pivot
     * @return \Illuminate\Http\Response
     */
    public function show(CustomClient_pivot $customClient_pivot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomClient_pivot  $customClient_pivot
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomClient_pivot $customClient_pivot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomClient_pivot  $customClient_pivot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomClient_pivot $customClient_pivot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomClient_pivot  $customClient_pivot
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomClient_pivot $customClient_pivot)
    {
        //
    }
}
