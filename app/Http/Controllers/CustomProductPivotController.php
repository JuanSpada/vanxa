<?php

namespace App\Http\Controllers;

use App\CustomProduct_pivot;
use Illuminate\Http\Request;

class CustomProductPivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomProduct_pivot  $customProduct_pivot
     * @return \Illuminate\Http\Response
     */
    public function show(CustomProduct_pivot $customProduct_pivot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomProduct_pivot  $customProduct_pivot
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomProduct_pivot $customProduct_pivot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomProduct_pivot  $customProduct_pivot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomProduct_pivot $customProduct_pivot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomProduct_pivot  $customProduct_pivot
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomProduct_pivot $customProduct_pivot)
    {
        //
    }
}
