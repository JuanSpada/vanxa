<?php

namespace App\Http\Controllers;

use App\CustomSale_pivot;
use Illuminate\Http\Request;

class CustomSalePivotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomSale_pivot  $customSale_pivot
     * @return \Illuminate\Http\Response
     */
    public function show(CustomSale_pivot $customSale_pivot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomSale_pivot  $customSale_pivot
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomSale_pivot $customSale_pivot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomSale_pivot  $customSale_pivot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomSale_pivot $customSale_pivot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomSale_pivot  $customSale_pivot
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomSale_pivot $customSale_pivot)
    {
        //
    }
}
