<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Sale;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $salesThisMonth  = Sale::where('company_id', Auth::user()->company_id)
                    ->whereMonth('created_at', '=', Carbon::now()->month)->get();
        $revenueThisMonth = 0;
        $salesAmmountThisYear = count($salesThisMonth);
        foreach ($salesThisMonth as $sale) {
            $revenueThisMonth += $sale->revenue; 
        }


        $revenueThisYear = 0;
        $salesThisYear  = Sale::where('company_id', Auth::user()->company_id)
                    ->whereYear('created_at', '=', Carbon::now()->year)->get();

        foreach ($salesThisYear as $sale) {
            $revenueThisYear += $sale->revenue; 
        }

        $salesTotal = count(Sale::where('company_id', Auth::user()->company_id)->get());

        $probando = 'proband';
        return view('dashboard', compact('probando'))
        ->with('revenueThisMonth', $revenueThisMonth)
        ->with('revenueThisYear', $revenueThisYear)
        ->with('salesAmmountThisYear', $salesAmmountThisYear)
        ->with('salesTotal', $salesTotal);
    }
}
    