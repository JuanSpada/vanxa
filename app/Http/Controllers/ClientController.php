<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Auth;
use App\CustomClient;
use App\CustomClient_pivot;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->get();
        $custom_fields = CustomClient::where('company_id', Auth::user()->company_id)->get();
        return view('clients.index')
        ->with('clients', $clients)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $custom_clients = CustomClient::where('company_id', Auth::user()->company_id)->get();
        return view('clients.create')
        ->with('custom_clients', $custom_clients);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'name.required' => 'Este campo es obligatorio',
                'name.min' => 'El nombre debe tener al menos 3 caracteres',
                'name.max' => 'El nombre no puede tener mas de 255 caracteres',
                'phone.numeric' => 'Este campo solo puede contener numeros',
                'phone.unique' => 'Este telefono ya ha sido usado',
                'email.unique' => 'Este email ya ha sido usado',
                'address.required' => 'Este campo es obligatorio',
        ];
        
        $validation = $request->validate([
            'name' => 'required|string|min:3|max:255',
            'phone' => 'nullable|numeric|unique:clients',
            'email' => 'nullable|unique:clients',
            'address' => 'required|max:255',
        ], $messages);        

        $client = new Client;
        $client->name = isset($request->name)? $request->name : null;
        $client->phone = isset($request->phone)? $request->phone : null;
        $client->email = isset($request->email)? $request->email : null;
        $client->address = isset($request->address)? $request->address : null;
        $client->info = isset($request->info)? $request->info : null;
        $client->company_id = Auth::user()->company_id;
        $client->save();

        if($request->custom_clients){
            /* CREAMOS LOS PRODUCTS VALUES EN LA TABLA PIVOT */
            foreach ($request->custom_clients as $i => $custom_client) {
            $custom_client_pivot = new CustomClient_pivot;
            $custom_client_pivot->custom_client_id = $custom_client['id'];
            $custom_client_pivot->company_id = Auth::user()->company_id;
            $custom_client_pivot->value = $custom_client['value'];
            $custom_client_pivot->client_id = $client->id;
            $custom_client_pivot->save();
            }
        }
        $clients = Client::where('company_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $custom_fields = CustomClient::where('company_id', Auth::user()->company_id)->get();
        return view('clients.index')
        ->with('clients', $clients)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = (int)$id;
        $client = Client::find($id);
        $custom_fields = CustomClient::where('company_id', Auth::user()->company_id)->get();

        // return view('clients.show')
        // ->with('client', $client)
        // ->with('custom_fields', $custom_fields);
        
        if (Auth::user()->company_id == $client->company_id) {
            return view('clients.show')
            ->with('client', $client)
            ->with('custom_fields', $custom_fields);
        }else {
            abort(403);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $messages = [
            'name.required' => 'Este campo es obligatorio',
            'name.min' => 'El nombre debe tener al menos 3 caracteres',
            'name.max' => 'El nombre no puede tener mas de 255 caracteres',
            'phone.numeric' => 'Este campo solo puede contener numeros',
            'phone.unique' => 'Este telefono ya ha sido usado',
            'email.unique' => 'Este email ya ha sido usado',
            'address.required' => 'Este campo es obligatorio',
    ];
    
    $validation = $request->validate([
        'name' => 'required|string|min:3|max:255',
        'phone' => 'nullable|numeric',
        'email' => 'nullable',
        'address' => 'required|max:255',
    ], $messages); 

        $client = Client::where('id', $id)->first();
        $client->name = $request->name;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->info = $request->info;
        $client->save();

        if($request->custom_fields){
            /* EDITAMOS EL PIVOT TABLE */
            foreach ($request->custom_fields as $custom_field) {
                $custom_client_pivot = CustomClient_pivot::where([
                    ['client_id', $custom_field['client_id']],
                    ['id', $custom_field['custom_client_id']],
                ])->get();
                $custom_client_pivot[0]->value = $custom_field['value'];
                $custom_client_pivot[0]->save();
            }
        }

        $clients = Client::where('company_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $custom_fields = CustomClient::where('company_id', Auth::user()->company_id)->get();
        return view('clients.index')
        ->with('clients', $clients)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $client = Client::where('id', $request->id);
        $client->delete();
        return back();
    }
}
