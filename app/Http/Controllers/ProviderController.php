<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;
use Auth;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Provider::where('company_id', Auth::user()->id)->get();
        return view('providers.index')->with('providers', $providers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider = new Provider;
        $provider->name = $request->name;
        $provider->company_id = Auth::user()->company_id;
        $provider->phone = $request->phone;
        $provider->address = $request->address;
        $provider->city = $request->city;
        $provider->info = $request->info;
        $provider->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::find($id);
        return view('providers.show')->with('provider', $provider);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Provider::find($id);
        return view('providers.edit')->with('provider', $provider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required' => 'Este campo es obligatorio',
            'name.min' => 'El nombre debe tener al menos 3 caracteres',
            'name.max' => 'El nombre no puede tener mas de 255 caracteres',
            'phone.numeric' => 'Este campo solo puede contener numeros',
            'phone.unique' => 'Este telefono ya ha sido usado',
            'email.unique' => 'Este email ya ha sido usado',
            'address.required' => 'Este campo es obligatorio',
        ];
    
        $validation = $request->validate([
            'name' => 'required|string|min:3|max:255',
            'phone' => 'nullable',
            'address' => 'required|max:255',
        ], $messages); 

        $provider = Provider::find($id);
        $provider->name = $request->name;
        $provider->company_id = Auth::user()->company_id;
        $provider->phone = $request->phone;
        $provider->address = $request->address;
        $provider->city = $request->city;
        $provider->info = $request->info;
        $provider->save();
        return redirect('/providers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provider = Provider::find($id);
        $provider->delete();
        return back();
    }
}
