<?php

namespace App\Http\Controllers;

use App\ProductProvider;
use Illuminate\Http\Request;
use Auth;

class ProductProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_provider = new ProductProvider;
        $product_provider->cost = $request->provider_cost;
        $product_provider->provider_id = $request->provider_id;
        $product_provider->product_id = $request->productId;
        $product_provider->company_id = Auth::user()->company_id;
        $product_provider->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductProvider  $productProvider
     * @return \Illuminate\Http\Response
     */
    public function show(ProductProvider $productProvider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductProvider  $productProvider
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductProvider $productProvider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductProvider  $productProvider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductProvider $productProvider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductProvider  $productProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $product_provider = ProductProvider::find($request->id);
        // dd($product_provider);
        $product_provider->delete();
        return back();
    }
}
