<?php

namespace App\Http\Controllers;

use App\Invite;
use Illuminate\Http\Request;

class InviteController extends Controller
{

    public function process()
    {
        // process the form submission and send the invite by email
    }

    public function accept($token)
    {
        // Look up the invite
        if (!$invite = Invite::where('token', $token)->first()) {
            //if the invite doesn't exist do something more graceful than this
            abort(404);
        }

        return view('invite')->with('invite', $invite);
    }
}
