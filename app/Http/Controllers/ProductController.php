<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Auth;
use App\CustomProduct;
use App\CustomProduct_pivot;
use App\Provider;
use App\ProductProvider;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('company_id', Auth::user()->company_id)->orderBy('created_at', 'desc')->get();
        $custom_fields = CustomProduct::where('company_id', Auth::user()->company_id)->get();
        return view('products.index')
        ->with('products', $products)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $providers = Provider::where('company_id', Auth::user()->company_id)->get();
        $custom_products = CustomProduct::where('company_id', Auth::user()->company_id)->get();
        return view('products.create')
        ->with('custom_products', $custom_products)
        ->with('providers', $providers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'Este campo es obligatorio',
            'price.required' => 'Este campo es obligatorio',
            'price.numeric' => 'Este campo solo puede contener numeros',
            // 'cost.required' => 'Este campo es obligatorio',
            // 'cost.numeric' => 'Este campo solo puede contener numeros',
        ];

        $validation = $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            // 'cost' => 'required|numeric',
        ], $messages);
        
        // dd($request);

        
        $product = new Product;
        $product->name = isset($request->name)? $request->name : null;
        $product->measure = isset($request->measure)? $request->measure : null;
        $product->price = isset($request->price)? $request->price : null;
        // $product->cost = isset($request->cost)? $request->cost : null;
        $product->time = isset($request->time)? $request->time : null;
        $product->company_id = Auth::user()->company_id;
        $product->save();

        /* CREAMOS LOS PRODUCTS VALUES EN LA TABLA PIVOT */
        if($request->custom_products){
            
            foreach ($request->custom_products as $i => $custom_product) {
                $custom_product_pivot = new CustomProduct_pivot;
                $custom_product_pivot->product_id = $product->id;
                $custom_product_pivot->company_id = Auth::user()->company_id;
                $custom_product_pivot->custom_product_id = $custom_product['id'];
                $custom_product_pivot->value = $custom_product['value'];
                $custom_product_pivot->save();
            }
            /*  */
        }

        /* CREAMOS LOS PROVEEDORES DEL PRODUCTO EN TABLA PIVOT */
        if ($request->providers) {
            foreach ($request->providers as $provider) {
                $product_provider = new ProductProvider;
                $product_provider->cost = $provider['cost'];
                $product_provider->product_id = $product->id;
                $product_provider->company_id = Auth::user()->company_id;
                $product_provider->provider_id = $provider['provider_id'];
                $product_provider->save();
            }
        }

        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = (int)$id;
        $product = Product::find($id);
        $custom_fields = CustomProduct::where('company_id', Auth::user()->company_id)->get();

        if (Auth::user()->company_id == $product->company_id) {
            return view('products.show')
            ->with('product', $product)
            ->with('custom_fields', $custom_fields);
        }else {
            abort(403);
        }

        return view('products.show')
        ->with('product', $product)
        ->with('custom_fields', $custom_fields);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = (int)$id;
        $product = Product::find($id);
        $custom_fields = CustomProduct::where('company_id', Auth::user()->company_id)->get();
        $product_provider = ProductProvider::where('product_id', $id)->get();
        $providers = Provider::where('company_id', Auth::user()->company_id)->get();
        // dd($product_provider);

        if (Auth::user()->company_id == $product->company_id) {
            return view('products.edit')
            ->with('product', $product)
            ->with('custom_fields', $custom_fields)
            ->with('product_provider', $product_provider)
            ->with('providers', $providers);
        }else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'name.required' => 'Este campo es obligatorio',
            'price.required' => 'Este campo es obligatorio',
            'price.numeric' => 'Este campo solo puede contener numeros',
            'cost.required' => 'Este campo es obligatorio',
            'cost.numeric' => 'Este campo solo puede contener numeros',
        ];

        $validation = $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'cost' => 'required|numeric',
        ], $messages);

        
        /* GUARDAMOS EL PRODUCTO */
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->cost = $request->cost;
        $product->measure = $request->measure;
        $product->time = $request->time;
        $product->save();

        /* EDITAMOS EL PIVOT TABLE */
        if ($request->custom_fields) {
            foreach ($request->custom_fields as $custom_field) {
                $custom_product_pivot = CustomProduct_pivot::where([
                    ['product_id', $custom_field['product_id']],
                    ['id', $custom_field['custom_product_id']],
                ])->get();
                $custom_product_pivot[0]->value = $custom_field['value'];
                $custom_product_pivot[0]->save();
            }
        }

        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /* ELIMINAMOS EL PRODUCTO */
        $products = Product::where('id', $request->id);
        $products->delete();

        /* ELIMINAMOS LA TABLA PIVOT DEL PRODUCTO */
        $custom_product_pivot = CustomProduct_pivot::where('product_id', $request->id)->get();
        foreach ($custom_product_pivot as $custom_fields) {
            $custom_fields->delete();
        }
        return back();
    }
}
