<?php

namespace App\Http\Controllers;

use App\CustomProduct;
use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\CustomProduct_pivot;

class CustomProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custom_products = CustomProduct::where('company_id', Auth::user()->id)->get();
        return view('customize.products.index')
                ->with('custom_products', $custom_products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_product = new CustomProduct;
        $custom_product->company_id = Auth::user()->company_id;
        $custom_product->value = $request->value;
        $custom_product->data_type = $request->data_type;
        $custom_product->save();

        /* Creamos los pivots para los clientes ya existentes */
        $products = Product::where('company_id', Auth::user()->company_id)->get();
        foreach ($products as $product) {
            $custom_product_pivot = new CustomProduct_pivot;
            $custom_product_pivot->custom_product_id = $custom_product->id;
            $custom_product_pivot->company_id = Auth::user()->company_id;
            $custom_product_pivot->value = null;
            $custom_product_pivot->product_id = $product->id;
            $custom_product_pivot->save();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomProduct  $customProduct
     * @return \Illuminate\Http\Response
     */
    public function show(CustomProduct $customProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomProduct  $customProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomProduct $customProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomProduct  $customProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomProduct $customProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomProduct  $customProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomProduct $customProduct, $id)
    {
        $custom_product = CustomProduct::find($id);
        $custom_product_pivots = CustomProduct_pivot::where([
            ['company_id', Auth::user()->company_id],
            ['custom_product_id', $id],
        ])->get();
        foreach ($custom_product_pivots as $custom_product_pivot) {
            $custom_product_pivot->delete();
        }
        $custom_product->delete();
        return back();
    }
}
