<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class ArtisanController extends Controller
{
    public function artisan(Request $request)
    {
        // EJ /artisan?command=migrate:fresh
        Artisan::call($request->command);
        dd(Artisan::output());
    }
}
