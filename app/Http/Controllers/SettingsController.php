<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use Auth;
use Illuminate\Support\Facades\Input;
use Mail;
use App\User;
use App\Mail\UserInvite;
use App\Invite;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::where('company_id', Auth::user()->company_id)->get();
        return view('settings')
        ->with('states', $states);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function invite()
    {
        dd('adUser');
    }

    public function settings(Request $request)
    {
        /* si invita un usuario */
        if(Input::has('invite-email')){
        
            do {
                //generate a random string using Laravel's str_random helper
                $token['token'] = str_random();
                $token['email'] = $request['invite-email'];
            } //check if the token already exists and if it does, try again
            while (Invite::where('token', $token)->first());
        
            //create a new invite record
            $invite = Invite::create([
                'email' => $request['invite-email'],
                'token' => $token['token'],
                'company_id' => Auth::user()->company_id,
            ]);
        
            // send the email
            Mail::to($request['invite-email'])->send(new UserInvite($invite));
        
            // redirect back where we came from
            return redirect()
            ->back();
        }

        /* si agrega un estado */
        if(Input::has('state-name')){
            $state = new State;
            $state->company_id = (int)Auth::user()->company_id;
            $state->name = $request['state-name'];
            $state->save();
            return back();
        }

        /* si edita el perfil */
        if(Input::has('user-name')){
            if($request['user-image']){
                $route = $request['user-image']->store('public');
                $fileName = basename($route);
            } else{ $fileName = "";}


            $user = User::find(Auth::user()->id);
            $user->img = $fileName;
            $user->name = $request['user-name'];
            $user->email = $request['user-email'];
            $user->save();
            return back();
        }

        /* si edita el la empresa */
        if(Input::has('company-name')){
            if($request['company-image']){
                $route = $request['company-image']->store('public');
                $fileName = basename($route);
            } else{ $fileName = "";}

            $company = Auth::user()->company;
            $company->img = $fileName;
            $company->name = $request['company-name'];
            $company->save();
            return back();
        }
    }

    public function registerInvite(Request $request)
    {
        /* FALTA VALIDACION DE PASSWORD CONFIRMATION */

        $user = User::create([
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'name' => $request['name'],
            'company_id' => (int)$request['company_id'],
        ]);

        /* ACA HAY QUE BORRAR EL INVITE ASI NO SE PUEDE VOLVERA  USAR */

        Auth::loginUsingId($user->id);

        return redirect('dashboard');
    }
}
