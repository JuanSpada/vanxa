<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomClient;

class CustomClient_pivot extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'custom_client_id', 'value', 'client_id',
    ];
    public function CustomClient()
    {
        $custom_client = CustomClient::where('id', $this->custom_client_id)->get();
        return $custom_client;
    }

}
