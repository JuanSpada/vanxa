<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CustomProduct_pivot;
use Auth;
use App\ProductProvider;

class Product extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'measure', 'price', 'cost', 'name', 'time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function sale(){
        return $this->belongsTo('App\Sale');
    }

    public function sales(){
        return $this->belongsToMany(Sale::class, 'product_sales');
    }

    public function customProducts(){
        $custom_products = CustomProduct_pivot::where('product_id', $this->id)->get();
        return $custom_products;
    }

    public function providers(){
        // $providers = ProductProvider::where('product_id', $this->id)->get();
        $providers = DB::table('providers')
            ->join('product_providers', 'providers.id', '=', 'product_providers.provider_id')
            ->where('product_providers.product_id', $this->id)
            ->select('providers.name', 'providers.phone', 'providers.address', 'providers.city', 'product_providers.cost', 'product_providers.info', 'product_providers.id')->get();
        // dd($providers);
        return $providers;
    }

    public function hasProviders(){
        $providers = ProductProvider::where('product_id', $this->id)->get();
        return $providers;
    }

}
