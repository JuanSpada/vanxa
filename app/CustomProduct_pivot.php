<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomProduct_pivot extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'custom_product_id', 'value', 'product_id',
    ];
    public function CustomProduct()
    {
        $custom_product = CustomProduct::where('id', $this->custom_product_id)->get();
        return $custom_product;
        // return $this->hasOne('App\CustomProduct');
    }
}