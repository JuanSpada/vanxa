<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductProviders;

class ProductSale extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'ammount', 'sale_id', 'product_id', 'price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function productProviders()
    {
        $product_providers_id = $this->product_providers_id;
        // $product_providers = 
        return $this->belongsTo('App\ProductProvider');
    }

    public function customProducts(){
        $custom_products = CustomProduct_pivot::where('product_id', $this->id)->get();
        return $custom_products;
    }

    
    // public function productProvider(){
    //     // $providers = ProductProvider::where('product_id', $this->id)->get();
    //     $product_provider = DB::table('product_providers')
    //         ->join('product_sales', 'product_providers.id', '=', 'product_sales.product_providers_id')
    //         ->where('product_providers.product_id', $this->id)
    //         ->select('providers.name', 'providers.phone', 'providers.address', 'providers.city', 'product_providers.cost', 'product_providers.info', 'product_providers.id')->get();
    //     // dd($providers);
    //     return $providers;
    // }
    
    // public function providers(){
    //     // $providers = ProductProvider::where('product_id', $this->id)->get();
    //     $providers = DB::table('providers')
    //         ->join('product_providers', 'providers.id', '=', 'product_providers.provider_id')
    //         ->where('product_providers.product_id', $this->id)
    //         ->select('providers.name', 'providers.phone', 'providers.address', 'providers.city', 'product_providers.cost', 'product_providers.info', 'product_providers.id')->get();
    //     // dd($providers);
    //     return $providers;
    // }
}
