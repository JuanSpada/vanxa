<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomSale_pivot extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'custom_sale_id', 'value', 'sale_id',
    ];

    public function CustomSale()
    {
        $custom_Sale = CustomSale::where('id', $this->custom_sale_id)->get();
        return $custom_Sale;
    }
}
