<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSaleProvider_pivot extends Model
{
    public function ProductSale()
    {
        $product_sale = ProductSale::where('id', $this->product_sales_id)->get();
        return $product_sale;
    }

    public function ProductProvider()
    {
        return $this->belongsTo('App\ProductProvider');
    }
}
