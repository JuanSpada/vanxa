<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomClient extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'value', 'data_type',
    ];

    public function client(){
        return $this->belongsTo('App\Client');
    }
}