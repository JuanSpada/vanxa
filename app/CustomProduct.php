<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomProduct extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'value', 'data_type',
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
