<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    
    Route::prefix('/settings')->name('settings.')->group(function() {
        Route::get('/', 'SettingsController@index');
        // Route::post('/', 'StateController@store')->name('store');
        // Route::post('/', 'StateController@store')->name('store');
        Route::post('/', 'SettingsController@settings')->name('settings');
        Route::prefix('/settings')->name('states.')->group(function() {
            Route::get('/{id}/destroy', 'StateController@destroy')->name('destroy');
        });
        Route::prefix('/customize')->name('customize.')->group(function() {
            Route::get('/', 'CustomizeController@index');
            //Falta armar los destroy de clients, products, y sales, pero tiene que validar si estan en uso a donde mover esos datos. o avisar que se van a borrar
            Route::prefix('/clients')->name('clients.')->group(function() {
                Route::get('/', 'CustomClientController@index');
                Route::post('/', 'CustomClientController@store')->name('store');
                Route::get('/{id}/destroy', 'CustomClientController@destroy')->name('destroy');
            });
            Route::prefix('/products')->name('products.')->group(function() {
                Route::get('/', 'CustomProductController@index');
                Route::post('/', 'CustomProductController@store')->name('store');
                Route::get('/{id}/destroy', 'CustomProductController@destroy')->name('destroy');
            });
            Route::prefix('/sales')->name('sales.')->group(function() {
                Route::get('/', 'CustomSaleController@index');
                Route::post('/', 'CustomSaleController@store')->name('store');
                Route::get('/{id}/destroy', 'CustomSaleController@destroy')->name('destroy');
            });
        });
    });

    Route::prefix('/sales')->name('sales.')->group(function() {
        Route::get('/destroy', 'SaleController@destroy')->name('destroy');
        Route::get('/', 'SaleController@index');
        Route::get('/new', 'SaleController@create')->name('create');
        Route::get('/{id}', 'SaleController@show')->name('show');
        Route::get('/{id}/edit', 'SaleController@edit')->name('edit');
        Route::post('/{id}/edit', 'SaleController@update')->name('update');
        Route::post('/new', 'SaleController@store')->name('store');
        Route::get('/{id}/edit/destroy', 'ProductSaleController@destroy')->name('product_destroy');
    });

    Route::prefix('/products')->name('products.')->group(function() {
        Route::get('/', 'ProductController@index');
        Route::get('/destroy', 'ProductController@destroy')->name('destroy');
        Route::get('/new', 'ProductController@create')->name('create');
        Route::get('/{id}/edit', 'ProductController@edit')->name('edit');
        Route::get('/{id}', 'ProductController@show')->name('show');
        Route::post('/new', 'ProductController@store')->name('store');
        Route::post('/{id}', 'ProductController@update')->name('update');
        Route::get('/{id}/edit/destroy', 'ProductProviderController@destroy')->name('provider_destroy');
        Route::post('/{id}/edit/add-provider', 'ProductProviderController@store')->name('add-provider');
    });

    Route::prefix('/clients')->name('clients.')->group(function() {
        Route::get('/destroy', 'ClientController@destroy')->name('destroy');
        Route::get('/', 'ClientController@index');
        Route::get('/new', 'ClientController@create')->name('create');
        Route::get('/{id}', 'ClientController@show')->name('show');
        Route::post('/new', 'ClientController@store')->name('store');
        Route::post('/{id}', 'ClientController@update')->name('update');
    });

    Route::prefix('/orders')->name('orders.')->group(function() {
        Route::get('/', 'SaleController@orders')->name('show_orders');
        Route::post('/', 'SaleController@orderUpdate')->name('orderUpdate');
    });

    Route::prefix('/providers')->name('providers.')->group(function() {
        Route::get('/', 'ProviderController@index')->name('index');
        Route::get('/{id}', 'ProviderController@show')->name('show');
        Route::get('/{id}/edit', 'ProviderController@edit')->name('edit');
        Route::post('/', 'ProviderController@store')->name('store');
        Route::post('/{id}', 'ProviderController@update')->name('update');
        Route::get('/destroy/{id}', 'ProviderController@destroy')->name('destroy');
        
    });
});

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/artisan', 'ArtisanController@artisan')->name('artisan');

Route::get('/accept/{token}', 'InviteController@accept')->name('accept');
Route::post('/accept/{token}', 'SettingsController@registerInvite')->name('registerInvite');