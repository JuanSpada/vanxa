<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Vanxa',
            'email' => 'info@vanxa.com',
            'img' => 'provo.jpg',
            'password' => bcrypt('vanxa'),
            'company_id' => 1,
        ]);
        DB::table('companies')->insert([
            'name' => 'Vanxa',
        ]);
        DB::table('products')->insert([
            'company_id' => 1,
            'measure' => '130 x 130',
            'price' => 20000,
            // 'cost' => 10000,
            'name' => 'Mesa Pinotea',
            'time' => 4,
        ]);
        DB::table('products')->insert([
            'company_id' => 1,
            'measure' => '130 x 130',
            'price' => 20000,
            // 'cost' => 10000,
            'name' => 'Mesa Guinea',
            'time' => 4,
        ]);
        DB::table('products')->insert([
            'company_id' => 1,
            'measure' => 0,
            'price' => 9000,
            // 'cost' => 3500,
            'name' => 'Silla Pata Copa',
            'time' => 2,
        ]);
        DB::table('products')->insert([
            'company_id' => 1,
            'measure' => 0,
            'price' => 8000,
            // 'cost' => 3500,
            'name' => 'Silla Petiribi',
            'time' => 2,
        ]);
        DB::table('products')->insert([
            'company_id' => 1,
            'measure' => 0,
            'price' => 7000,
            // 'cost' => 2500,
            'name' => 'Silla Capitonee',
            'time' => 2,
        ]);
        DB::table('states')->insert([
            'company_id' => 1,
            'name' => 'Nuevo',
        ]);
        DB::table('states')->insert([
            'company_id' => 1,
            'name' => 'En Progreso',
        ]);
        DB::table('states')->insert([
            'company_id' => 1,
            'name' => 'Entregado',
        ]);

        // CREAMOS PRODUCTPROVIDERS
        
        // SEDEEA MAL LAS SALES
        // $sales = factory(App\Sale::class, 10)->create(); //NO ANDA POR Q FALTA CREAR LA TABLA PIVOT 
        // $productsales = factory(App\ProductSale::class, 10)->create();
        // $users = factory(App\User::class, 10)->create();
        $clients = factory(App\Client::class, 10)->create();
        $providers = factory(App\Provider::class, 10)->create();
        $providers = factory(App\ProductProvider::class, 10)->create();
        
    }
}
