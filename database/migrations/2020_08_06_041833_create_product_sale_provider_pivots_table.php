<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSaleProviderPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sale_provider_pivots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('company_id')->unsigned();
            $table->bigInteger('product_sales_id')->unsigned();
            $table->bigInteger('provider_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('users');
            $table->foreign('product_sales_id')->references('id')->on('product_sales');
            $table->foreign('provider_id')->references('id')->on('providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sale_provider_pivots');
    }
}
