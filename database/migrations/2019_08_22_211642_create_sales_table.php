<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('company_id');
            $table->integer('client_id');
            $table->integer('state')->nullable();
            $table->float('downpayment')->nullable();
            $table->float('cost')->nullable();
            $table->date('date')->nullable();
            $table->float('revenue')->nullable();
            $table->string('source')->nullable();
            $table->string('info')->nullable();
            $table->float('discount')->nullable();
            $table->string('img')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
