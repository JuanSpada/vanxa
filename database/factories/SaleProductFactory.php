<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SaleProduct;
use Faker\Generator as Faker;

$factory->define(SaleProduct::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 3),
        'sale_id' => rand(1, 3),
        'product_id' => rand(1, 3),
    ];
});
