<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sale;
use Faker\Generator as Faker;

$factory->define(Sale::class, function (Faker $faker) {
    return [
        'company_id' => rand(1,2),
        'client_id' => rand(1, 10),
        'state' => rand(1, 3),
        'downpayment' => rand(500, 3000),
        'cost' => rand(2000, 10000),
        'date' => now(),
        'source' => $faker->name,
        'info' => $faker->name,
    ];
});