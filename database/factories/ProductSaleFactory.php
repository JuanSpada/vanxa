<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductSale;
use Faker\Generator as Faker;

$factory->define(ProductSale::class, function (Faker $faker) {
    return [
        'ammount' => rand(1, 5),
        'sale_id' => rand(1, 2),
        'company_id' => 1,
        'product_id' => rand(1, 2),
    ];
});