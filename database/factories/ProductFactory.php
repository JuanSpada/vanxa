<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'measure' => rand(1, 200) . ' x ' . rand(1, 200),
        'user_id' => rand(1, 3),
        'price' => rand(7000, 50000),
        'cost' => rand(3000, 10000),
        'time' => rand(1, 20),
    ];
});
