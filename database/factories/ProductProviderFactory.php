<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductProvider;
use Faker\Generator as Faker;

$factory->define(ProductProvider::class, function (Faker $faker) {
    return [
        'company_id' => 1,
        'info' => 'Info extra bla bla bla',
        'cost' => rand(999, 5000),
        'product_id' => rand(1, 5),
        'provider_id' => rand(1, 5),
    ];
});
