<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Provider;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' =>$faker->phoneNumber,
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'company_id' => 1,
    ];
});
